from django.shortcuts import render
from .models import Data
# Create your views here.
def home(request):
    project = Data.objects.all()
    return render(request , 'home/home.html' , {'project':project})