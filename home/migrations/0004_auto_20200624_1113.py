# Generated by Django 3.0.7 on 2020-06-24 06:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_data_email'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='data',
            name='email',
        ),
        migrations.RemoveField(
            model_name='data',
            name='image',
        ),
        migrations.RemoveField(
            model_name='data',
            name='text',
        ),
        migrations.AddField(
            model_name='data',
            name='description',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='data',
            name='name',
            field=models.CharField(default='', max_length=50),
        ),
    ]
